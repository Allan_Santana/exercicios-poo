import java.util.Scanner;


public class exercicio_2 {

    Scanner sc = new Scanner(System.in);
    int cels;

    public exercicio_2() {
        System.out.println("Entre com a temperatura Celsius: ");
        setCels(sc.nextInt());
        System.out.println("A Temperatura convvertida � de: " + getCels());
    }

    public int getCels() {
        return cels;
    }

    public void setCels(int cels) {
        this.cels = (cels - 32) * 5 / 9;
    }
}
class TemperaturaTeste extends exercicio_2 {

    public static void main(String[] args) {
        new exercicio_2();
    }
}




