 import java.util.Scanner;// importar uma palavra desse pacote
                          // com * pode utilizar qualquer uma

public class exercicio1 {

	static final double PI = 3.14;  // o que faz ela ser uma constante � o final
			                      // o que faz ser uma constante global � o static
		
    public static void main (String[] args) {
		// Declara��o e inicializa��o das variaveis e constantes
		double raio, area;
		
		raio = 0;
		area = 0;
		Scanner tecla = new Scanner (System.in);// new usava na cria��o de objetos
		
		// Entrada de dados
		System.out.println("Informe o raio do circulo:");
		raio = tecla.nextDouble();
		
		// Processamento de daos
		area = PI * Math.sqrt(raio);
		
		// Sa�da da informa��o
		System.out.println("A area do circulo �: " + area);
		
	}

}
